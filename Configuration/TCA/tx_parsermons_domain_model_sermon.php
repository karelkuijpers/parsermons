<?php
if (!defined ('TYPO3')) 	die ('Access denied.');

return Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon",
		"label" => "title",
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
//		'dividers2tabs' => $confArr['noTabDividers']?FALSE:TRUE,
		"default_sortby" => "ORDER BY occurrence_date DESC",
		"delete" => "deleted",
		"enablecolumns" => Array (
			"disabled" => "hidden",
			"starttime" => "starttime",
			"endtime" => "endtime",
			"fe_group" => "fe_group",
		),
        'searchFields' => 'uid,title',
	),
	"columns" => Array (
		"hidden" => Array (
			"exclude" => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
		"starttime" => Array (
			"exclude" => 1,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
			"config" => Array (
				"type" => "input",
				'renderType' => 'inputDateTime',
				"size" => "8",
				"eval" => "date",
				"default" => "0",
				"checkbox" => "0"
			)
		),
		"endtime" => Array (
			"exclude" => 1,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
			"config" => Array (
				"type" => "input",
				'renderType' => 'inputDateTime',
				"size" => "8",
				"eval" => "date",
				"checkbox" => "0",
				"default" => "0",
				"range" => Array (
					"upper" => mktime(0,0,0,12,31,2020),
					"lower" => mktime(0,0,0,date("m")-1,date("d"),date("Y"))
				)
			)
		),
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 20,
                'items' => [
                    ['label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login', 'value' => -1],
                    ['label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login', 'value' => -2],
                    ['label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups', 'value' => '--div--'],
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
                'foreign_table_where' => 'ORDER BY fe_groups.title',
            ],
        ],
		"title" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.title",
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"eval" => "required",
			)
		),
		"occurrence_date" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.occurrence_date",
			"config" => Array (
				"type" => "input",
				'renderType' => 'inputDateTime',
				"size" => "8",
				"eval" => "date",
				"checkbox" => "0",
				"default" => "0"
			)
		),
		"description" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.description",
			"config" => Array (
				"type" => "text",
				"cols" => "30",
				"rows" => "5",
				"wizards" => Array(
					"_PADDING" => 2,
					"RTE" => Array(
						"notNewRecords" => 1,
						"RTEonly" => 1,
						"type" => "script",
						"title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
						"icon" => "actions-wizard-rte",
						"module" => array('wizard_rte',),
//						"script" => "wizard_rte.php",
					),
				),
			)
		),
		"resources" => Array (
            'exclude' => true,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.resources",
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'resources',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.resources.add",
                        'showSynchronizationLink' => true,
			'fileUploadAllowed' => 0
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'resources',
                        'tablenames' => 'tx_parsermons_domain_model_sermon',
                   ],
					'maxitems'=> 9,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext'].',pptx'
            ),
        ), 
		"speakers" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_sermon.speakers",
            'config' => [
                'type' => 'group',
                'allowed' => 'tx_parsermons_domain_model_speaker',
                'size' => 5,
                'autoSizeMax' => 10,
                'foreign_table' => 'tx_parsermons_domain_model_speaker',
                'foreign_table_where' => 'ORDER BY tx_parsermons_domain_model_speaker.fullname',
                'MM' => 'tx_parsermons_sermon_speaker_mm',
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                        'options' => [
                            'setValue' => 'prepend',
                        ],
                    ],
                ],
          ],


/*			"config" => Array (
				"type" => "inline",
//                'internal_type' => 'db',
				'allowed' => 'tx_parsermons_domain_model_speaker', 
				'default' => NULL,
//				'foreign_table' => 'tx_parsermons_domain_model_speaker',
				"mm" => "tx_parsermons_sermon_speaker_mm", 
				'MM_hasUidField' => true,
				'foreign_table' => "tx_parsermons_sermon_speaker_mm", 
				"foreign_field" => "uid_local",
				"foreign_label" => "uid_foreign",
				"foreign_selector" => "uid_foreign",
				"foreign_unique" => "uid_foreign",

				"appearance" => Array (
					"newRecordLinkAddTitle" => "1",
					"expandSingle" => "1",
					"collapseAll" => "1",
					"useCombination" => "1"
				),
				"size" => 4,
				"minitems" => 0,
				"maxitems" => 5,
			) */
		), 
	),
	"types" => Array (
		"0" => Array(
			"columnsOverrides" => Array(
				"description"=> Array(
					"config" => Array(
					"enableRichtext" => 1, "richtextConfiguration" => "default")
				)
			),
		"showitem" => "hidden,--palette--;;1,--palette--;;2,occurrence_date,description,resources,speakers"),
	),
	"palettes" => Array (
		"1" => Array("showitem" => "starttime,endtime,fe_group,islinked"),
		"2" => Array("showitem" => "title"),
	)
);