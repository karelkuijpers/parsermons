<?php
if (!defined ('TYPO3')) 	die ('Access denied.');

return Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xml:tx_parsermons_domain_model_speaker",
		"label" => "fullname",
		"label_alt" => "lastname, uid",
		"label_alt_force" => FALSE,
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"default_sortby" => "ORDER BY fullname",
		"delete" => "deleted",
		"enablecolumns" => Array (
			"disabled" => "hidden",
		),
	),
	"columns" => Array (
		"hidden" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
		"fullname" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.fullname",
			"config" => Array (
				"type" => "input",
				"size" => "40",
				"max" => "80",
				"eval" => "required",
			)
		),
		"firstname" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.firstname",
			"config" => Array (
				"type" => "input",
				"size" => "15",
				"max" => "50",
				"eval" => "",
			)
		),
		"lastname" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.lastname",
			"config" => Array (
				"type" => "input",
				"size" => "15",
				"max" => "50",
				"eval" => "",
			)
		),
		"email" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.email",
			"config" => Array (
				"type" => "input",
				"size" => "30",
			)
		),
		"photo" => Array (
            'exclude' => true,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.photo",
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'photo',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.photo.add",
                        'showSynchronizationLink' => true,
			'fileUploadAllowed' => 0
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'photo',
                        'tablenames' => 'tx_parsermons_domain_model_speaker',
                    ],
    		    'maxitems'=> 1,
                ],
               'jpg,gif,svg,png,bmp,jpeg'
            )  
        ),
		"blogurl" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:parsermons/Resources/Private/Language/locallang_db.xlf:tx_parsermons_domain_model_speaker.blogurl",
			"config" => Array (
				"type" => "input",
				"size" => "50"
			)
		)
	),
	"types" => Array (
		"0" => Array(
			"columnsOverrides" => Array(
				"description"=> Array(
					"config" => Array(
					"enableRichtext" => 1, "richtextConfiguration" => "default")
				)
			),
			"showitem" => "hidden,--palette--;;1,fullname,--palette--;;2,email,blogurl,photo")
	),
	"palettes" => Array (
		"1" => Array("showitem" => ""),
		"2" => Array("showitem" => "firstname, lastname"),
	)
);

