<?php
defined('TYPO3') or die();

/***************
 * Plugin
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Parsermons',
    'Sermon',
    'Par Sermon Management System'
);

//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['parsermons_sermon'] = 'recursive';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['parsermons_sermon'] = 'pi_flexform';
/*\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('parsermons_sermon',
    'FILE:EXT:parsermons/Configuration/FlexForms/flexform_sermon.xml'); 

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('tx_parsermons_sermon'); */
