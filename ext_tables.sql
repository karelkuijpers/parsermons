#
# Table structure for table 'tx_parsermons_domain_model_sermon'
#
CREATE TABLE tx_parsermons_domain_model_sermon (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	sorting int(10) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group int(11) NULL DEFAULT NULL,
	title tinytext NULL,
	occurrence_date int(11) DEFAULT '0' NOT NULL,
	description text NOT NULL,
	resources int(11) unsigned DEFAULT '0',
	speakers int(11) NULL DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)
) DEFAULT CHARACTER SET utf8;



#
# Table structure for table 'tx_parsermons_domain_model_speaker'
#
CREATE TABLE tx_parsermons_domain_model_speaker (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	fullname tinytext NOT NULL,
	firstname tinytext NOT NULL,
	lastname tinytext NOT NULL,
	photo int(11) unsigned DEFAULT '0',
	email tinytext NOT NULL,
	blogurl tinytext NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)
);
#
# Table structure for table 'tx_parsermons_sermon_speaker_mm'
#
CREATE TABLE tx_parsermons_sermon_speaker_mm (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	uid_local  int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
    sorting         int(11) unsigned DEFAULT '0' NOT NULL,
    sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);

