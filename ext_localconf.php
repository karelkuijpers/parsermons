<?php
//      \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('parsermons', 'Configuration/TypoScript', 'Sermons');
/*
      \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_parsermons_domain_model_sermon', 'EXT:parsermons/Resources/Private/Language/locallang_csh_tx_parsermons_domain_model_sermon.xlf');
      \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_parsermons_domain_model_sermon');

      \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_parsermons_domain_model_speaker', 'EXT:parsermons/Resources/Private/Language/locallang_csh_tx_parsermons_domain_model_speaker.xlf');
      \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_parsermons_domain_model_speaker'); */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Parsermons',
         'Sermon',
         [
             \Parousia\Parsermons\Controller\SermonController::Class => 'list, show'
         ],
         // non-cacheable actions
         [
             \Parousia\Parsermons\Controller\SermonController::Class => 'list, show'
         ]
     );


