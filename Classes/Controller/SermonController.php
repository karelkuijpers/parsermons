<?php
namespace Parousia\Parsermons\Controller;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/
use Parousia\Parsermons\Domain\Model\Sermon;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

/**
 * SermonController
 */
class SermonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Parousia\Parsermons\Domain\Repository\SermonRepository
     */
    protected $SermonRepository;
	protected $args;
	protected $general;
	protected $uid;
	
    /**
     * Inject a sermon repository to enable DI
     *
     * @param \Parousia\Parsermons\Domain\Repository\SermonRepository $sermonRepository
     */
    public function injectSermonRepository(\Parousia\Parsermons\Domain\Repository\SermonRepository $sermonRepository)
    {
        $this->SermonRepository = $sermonRepository;
    }
    /**
     * Initialize redirects
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		if (array_key_exists('general',$this->args))$this->general = (string)$this->args['general'];
		if (array_key_exists('uid',$this->args))$this->uid = $this->args['uid'];
	}
	
    /**
     * action list
     *
     * @return void
     */
    public function listAction(): ResponseInterface
    {
		//$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'listAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');
		//$general=$this->args['general'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Parsermons sermons: general '.$general."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');

        $sermons = $this->SermonRepository->findSelection($this->general);
    	$assignedValues = [
			'general' => $this->general,
			'sermons' => $sermons,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param int $uid
     * @return void
     */
    public function showAction(): ResponseInterface
    {
	//	var_dump("show:".$uid);
        $sermon = $this->SermonRepository->findByUid($this->uid);
    	$assignedValues = [
			'sermon' => $sermon,
			'general' => $this->general,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    } 


}
