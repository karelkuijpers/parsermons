<?php
namespace Parousia\Parsermons\Domain\Model;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A sermon is given by a speaker on a certain date
 */
class Sermon extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title of a sermon
     *
     * @var string
     */
    protected $title = '';

    /**
     * The date this sermon was presented
     *
     * @var \DateTime
     */
    protected $occurrenceDate = null;

    /**
     * The general description given for this sermon
     *
     * @var string
     */
    protected $description = '';

    /**
     * time of last update record
     *
     * @var \DateTime
     */
    protected $tstamp = null;

    /**
     * indicates if record was deleted
     *
     * @var bool
     */
    protected $deleted = false;

    /**
     * indicates that the record is hidden in frontend
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * date of creation of the record
     *
     * @var \DateTime
     */
    protected $crdate = null;

    /**
     * the backend user who created this record
     *
     * @var int
     */
    protected $cruserId = 0;
	
    /**
     * @var int
     */
    protected $uid;

    /**
     * the frontend usersgroup which has access to thsi domain
     *
     * @var string
     */
    protected $feGroup = '';

    /**
     * starttime
     *
     * @var \DateTime
     */
    protected $starttime = null;

    /**
     * endtime
     *
     * @var \DateTime
     */
    protected $endtime = null;

    /**
     * sysLanguageUid
     *
     * @var int
     */
    protected $sysLanguageUid = 0;

    /**
     * @var int
     */
    protected $l10nParent;

    /**
     * the FAL-file with the mp3 of the sermon
     *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Parsermons\Domain\Model\FileReference>
	*/
    protected $resources;

    /**
     * The person who gave the sermon
     *
     * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Parsermons\Domain\Model\Speaker
     */
    protected $speakers = null;
    /**
     * Initialize sermon
     *
     * @return \Parousia\Parsermons\Domain\Model\Sermon
     */
    public function __construct()
    {
        $this->resources = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->speakers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the occurrenceDate
     *
     * @return \DateTime $occurrenceDate
     */
    public function getOccurrenceDate()
    {
        return $this->occurrenceDate;
    }

    /**
     * Sets the occurrenceDate
     *
     * @param \DateTime $occurrenceDate
     * @return void
     */
    public function setOccurrenceDate(\DateTime $occurrenceDate)
    {
        $this->occurrenceDate = $occurrenceDate;
    }
	    /**
     * Returns the sermonFile
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getResources()
    {
        return $this->resources;
    }


    /**
     * Short method for getMedia()
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getMedia()
    {
        return $this->getResources();
    }

    /**
     * Set Resources relation
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $resources
     */
    public function setResources(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $resources)
    {
        $this->resources = $resources;
    }

    /**
     * Add a resources file reference
     *
     * @param FileReference $resources
     */
    public function addResources(FileReference $resources)
    {
        if ($this->getResources() === null) {
            $this->resources = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->resources->attach($resources);
    }

    /**
     * Get all media elements which are allowed for detail views
     *
     * @return array
     */
    public function getMediaNonPreviews()
    {
        return $this->getMediaDetailOnly();
    }

    /**
     * Get all media elements which are only for detail views
     *
     * @return array
     */
    public function getMediaDetailOnly()
    {
        foreach ($this->getResources() as $mediaItem) {
        /** @var $mediaItem FileReference */
             $items[] = $mediaItem;
        }
        return $items;
    }


    /**
     * Get first non preview
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference|null
     */
    public function getFirstNonePreview()
    {
        foreach ($this->getMediaNonPreviews() as $mediaElement) {
            return $mediaElement;
        }
        return null;
    }


    /**
     * Returns the speaker
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage	 
     */
    public function getSpeakers()
    {
        return $this->speakers;
    }

    /**
     * Sets the speaker
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $speakers speakers
     */
    public function setSpeakers($speakers)
    {
        $this->speakers = $speakers;
    }
	    /**
     * Adds a tag
     *
     * @param \Parousia\Parsermons\Domain\Model\Speaker $speaker
     */
    public function addSpeaker(\Parousia\Parsermons\Domain\Model\Speaker $speaker)
    {
        $this->speakers->attach($speaker);
    }

    /**
     * Removes a speaker
     *
     * @param \Parousia\Parsermons\Domain\Model\Speaker $speaker
     */
    public function removeSpeaker(\Parousia\Parsermons\Domain\Model\Speaker $speaker)
    {
        $this->speakers->detach($speaker);
    }

    /**
     * Returns the tstamp
     *
     * @return \DateTime $tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Sets the tstamp
     *
     * @param \DateTime $tstamp
     * @return void
     */
    public function setTstamp(\DateTime $tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     * @return void
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Returns the boolean state of deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Returns the hidden
     *
     * @return bool $hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden
     *
     * @param bool $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the boolean state of hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * Returns the crdate
     *
     * @return \DateTime $crdate
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Sets the crdate
     *
     * @param \DateTime $crdate
     * @return void
     */
    public function setCrdate(\DateTime $crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * Returns the cruserId
     *
     * @return int $cruserId
     */
    public function getCruserId()
    {
        return $this->cruserId;
    }
    /**
     * Sets the cruserId
     *
     * @param int $cruserId
     * @return void
     */
    public function setCruserId($cruserId)
    {
        $this->cruserId = $cruserId;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Returns the feGroup
     *
     * @return string $feGroup
     */
    public function getFeGroup()
    {
        return $this->feGroup;
    }

    /**
     * Sets the feGroup
     *
     * @param string $feGroup
     * @return void
     */
    public function setFeGroup($feGroup)
    {
        $this->feGroup = $feGroup;
    }

    /**
     * Returns the starttime
     *
     * @return \DateTime $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param \DateTime $starttime
     * @return void
     */
    public function setStarttime(\DateTime $starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the endtime
     *
     * @return \DateTime $endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Sets the endtime
     *
     * @param \DateTime $endtime
     * @return void
     */
    public function setEndtime(\DateTime $endtime)
    {
        $this->endtime = $endtime;
    }

    /**
     * Returns the sysLanguageUid
     *
     * @return int $sysLanguageUid
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * Sets the sysLanguageUid
     *
     * @param int $sysLanguageUid
     * @return void
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;
    }
}
