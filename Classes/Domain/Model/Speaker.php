<?php
namespace Parousia\Parsermons\Domain\Model;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A speaker is a person who does the preaching
 */
class Speaker extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    protected $tstamp = null;

    /**
     * firstname
     *
     * @var string
     */
    protected $firstname = '';

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * fullname
     *
     * @var string
     */
    protected $fullname = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * deleted
     *
     * @var bool
     */
    protected $deleted = false;

    /**
     * hidden
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * starttime
     *
     * @var \DateTime
     */
    protected $starttime = null;

    /**
     * endtime
     *
     * @var \DateTime
     */
    protected $endtime = null;

    /**
     * photo
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    protected $photo = null;

    /**
     * blogurl
     *
     * @var string
     */
    protected $blogurl = '';

    /**
     * Returns the firstName
     *
     * @return string $firstName
     */

    /**
     * Initialize media relation
     *
     * @return \Parousia\Parsermons\Domain\Model\Speaker
     */
    public function __construct()
    {
        $this->photo = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

	 /**
     * Returns the fullname
     *
     * @return string $lastname
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     * @return void
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the fullname
     *
     * @return string $fullname
     */
    public function getFullName()
    {
        return $this->fullname;
    }

    /**
     * Sets the fullname
     *
     * @param string $fullname
     * @return void
     */
    public function setFullName($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * Returns the blogurl
     *
     * @return string $blogurl
     */
    public function getBlogUrl()
    {
        return $this->blogurl;
    }

    /**
     * Sets the blogurl
     *
     * @param string $blogurl
     * @return void
     */
    public function setBlogUrl($blogurl)
    {
        $this->blogurl = $blogurl;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the photo
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Sets the Photo
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * Add a resources file reference
     *
     * @param FileReference $photo
     */
    public function addResouces(FileReference $photo)
    {
        if ($this->getPhoto() === null) {
            $this->photo = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->photo->attach($photo);
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     * @return void
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Returns the boolean state of deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Returns the tstamp
     *
     * @return \DateTime $tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Sets the tstamp
     *
     * @param \DateTime $tstamp
     * @return void
     */
    public function setTstamp(\DateTime $tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the hidden
     *
     * @return bool $hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden
     *
     * @param bool $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the boolean state of hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * Returns the starttime
     *
     * @return \DateTime $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param \DateTime $starttime
     * @return void
     */
    public function setStarttime(\DateTime $starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the endtime
     *
     * @return \DateTime $endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Sets the endtime
     *
     * @param \DateTime $endtime
     * @return void
     */
    public function setEndtime(\DateTime $endtime)
    {
        $this->endtime = $endtime;
    }
}
