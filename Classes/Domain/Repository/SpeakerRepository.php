<?php

namespace Parousia\Parsermons\Domain\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;


/**
 * Class ProductRepository
 *
 * @package TYPO3\CMS\Extbase\Persistence\Repository
 */
class SpeakerRepository extends Repository
{
	public function findAll()
	{
		$query = $this->createQuery();

 	//	$queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
	//	 \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL());
	//	 \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getParameters()); 

        return $query->execute();
    }
}
