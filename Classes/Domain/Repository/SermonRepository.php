<?php

namespace Parousia\Parsermons\Domain\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class SermonRepository
 *
 * @package TYPO3\CMS\Extbase\Persistence\Repository
 *
 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class SermonRepository extends Repository
{
	public function findAll()
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);

		$occurrencelimit = new \DateTime("- 2 years");

		$statement='SELECT s.uid,s.pid,s.title,s.occurrence_date as occurrenceDate,group_concat(sp.fullname) as fullname FROM `tx_parsermons_domain_model_sermon` s left join `tx_parsermons_sermon_speaker_mm` mm on (mm.uid_local=s.uid) left join `tx_parsermons_domain_model_speaker` sp on (mm.uid_foreign=sp.uid)';
		$query->statement($statement.' where s.deleted=0 and s.hidden=0 and s.occurrence_date > '.$occurrencelimit->getTimestamp().'  group by s.uid order by s.occurrence_date desc');
//		$qstatement=$query->getStatement()->getStatement;
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SermonRepository findAll statement: '.$qstatement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }
	
		
	public function findSelection(string $search = null)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$statement='SELECT s.uid,s.pid,s.title,s.occurrence_date as occurrenceDate,group_concat(sp.fullname) as fullname FROM `tx_parsermons_domain_model_sermon` s left join `tx_parsermons_sermon_speaker_mm` mm on (mm.uid_local=s.uid) left join `tx_parsermons_domain_model_speaker` sp on (mm.uid_foreign=sp.uid)';
		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SermonRepository findSelection statement: '.$statement.' where '.$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');		
		if (empty($search))	$query->statement($statement.' where '.$zoekargument);
		else $query->statement($statement.' where '.$zoekargument);
		
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }

	 /**
     * Override default findByUid function 
     *
     * @param int  $uid                 id of record
     *
     * @return Preek
     */
    public function findByUid($uid)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
        $statement='SELECT s.title,s.occurrence_date as occurrenceDate,s.description,sp.fullname,sp.firstname,sp.lastname,sp.uid as spuid FROM `tx_parsermons_domain_model_sermon` s '.
        'left join `tx_parsermons_sermon_speaker_mm` mm on (mm.uid_local=s.uid) left join `tx_parsermons_domain_model_speaker` sp on (mm.uid_foreign=sp.uid)';
		$query->statement($statement.' where s.uid= ?', [$uid]);
        $results= $query->execute(true);
//		var_dump($results[0]);
		$result=$results[0];
		$sermon = new \Parousia\Parsermons\Domain\Model\Sermon();
		$sermon->setTitle($result['title']);
		$sermon->setDescription($result['description']);
		$date=new \DateTime();
		$date->setTimestamp($result['occurrenceDate']);
		$sermon->setOccurrenceDate($date);
		
		// find sermon-files:
		$statement='SELECT r.uid_local,r.uid,r.title from `sys_file_reference` r where r.fieldname="resources" and r.deleted=0 and r.uid_foreign = '.$uid.' order by r.sorting_foreign';
		$query->statement($statement);
                $sysrefs= $query->execute(true);
	//	var_dump($sysrefs);
		foreach ($sysrefs as $sysref)
		{
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'uid_local : '.$sysref['uid_local'].'; title:'.$sysref['title']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.log');

			$fileref = new \Parousia\Parsermons\Domain\Model\FileReference();
			$fileref->setFileUid($sysref['uid_local']);
			$fileref->_setProperty('uid',$sysref['uid']);
			$fileref->setTitle($sysref['title']);
		//	$title=$fileref->getTitle();
		//	$fileref->setTitle($title);
			$sermon->addResources($fileref);
		}
        $statement='SELECT sp.fullname,sp.uid as spuid FROM '.
                '`tx_parsermons_sermon_speaker_mm` as mm left join `tx_parsermons_domain_model_speaker` sp on (mm.uid_foreign=sp.uid)';
		$query->statement($statement.' where mm.uid_local= '.$uid);
        $results= $query->execute(true);
		foreach ($results as $result)
		{
			$speaker=new \Parousia\Parsermons\Domain\Model\Speaker();
			$speaker->setFullName($result['fullname']);
			// picture speaker:
			$statement='SELECT r.uid_local,r.uid from `sys_file_reference` r where r.fieldname="photo" and r.deleted=0 and r.uid_foreign = '.$result['spuid'];
			$query->statement($statement);
            $sysrefs= $query->execute(true);
			foreach ($sysrefs as $sysref)
			{
				$fileref = new \Parousia\Parsermons\Domain\Model\FileReference();
				$fileref->setFileUid($sysref['uid_local']);
				$fileref->_setProperty('uid',$sysref['uid']);
				$speaker->addResouces($fileref);
			}
			$sermon->addSpeaker($speaker);
		}
		//var_dump($sermon);
		return $sermon;
	}

		/**
	 * Method 'ComposeSearch' for the 'parsermon' extension.
 	*
	 * param searchfield: keyword required persons ($search)
	 * returns composed sql search parameter
	 */
	function ComposeSearch(string $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		$zoekargument="";
		$Not=false;
		$occurrencelimit = new \DateTime("- 2 years");
		
		$connection=\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if ($search<>""){ 
			$a_key=$this->DeriveKeywords($search);
				//	TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekveld:".$Zoekveld[0],"churchauthreg");
			foreach ($a_key as $key)
			{	$key=trim($key);
				if (!empty($key))
				{	// key:
					if (substr($key,0,1)=="-")
					{	
						$zoekargument.="NOT ";  // ontkenning
						$key=substr($key,1);
						$Not=true;
					}
					$keystring=$key;
					if (strlen($keystring)>=8 && strlen($keystring)<=10 && strtotime($key))
					{
						$keydate=new \DateTime($key);
						if (is_object($keydate))
						{
							$keystring=$keydate->format("Y-m-d");
							//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeSearch keydate UTC : '.$keydate->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');						
						}
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeSearch keystring : '.$keystring."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parsermons/Classes/Controller/debug.txt');						
					}
					$key=addslashes($key);
						//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekwoord:".$key,"churchauthreg");
					$zoekargument.=' (sp.fullname like "%'.$key.'%" OR ';
					// ook op andere velden zoeken:
					$zoekargument .=  "s.title LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "s.description LIKE '%" . $key ."%' OR ";
					$zoekargument .=  " DATE_FORMAT(CONVERT_TZ(from_unixtime(s.occurrence_date),'+00:00', '+01:00'), '%Y-%m-%d') like '%". $keystring ."%') AND ";
				}
			}
		}
		$zoekargument.=' s.deleted=0 and s.hidden=0 '.(empty($search) ? 'and s.occurrence_date > '.$occurrencelimit->getTimestamp() : '').'  group by s.uid order by s.occurrence_date desc';
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	/*
	 * function DeriveKeywords:
	 * Derive search keywords from a Google-like searchfield $strZoek
	 */
	function DeriveKeywords($strZoek){
		// bepalen van zoekwoorden uit een vrije tekst veld
		$aZoekwoord=array();
		$aZoekveld=array();
		$Quote="";
		$WordQ="";
		$Min="";
		// Replace &quote; back to ":
		$strZoek=str_ireplace("&quot;", '"', $strZoek);
		// splits in losse woorden:
		$aZoekwoord = preg_split ("/[\s,;]+/", trim($strZoek)); 
		// voeg strings tussen quote's weer samen:
		for ($i=0;$i<count($aZoekwoord);++$i)
		{	$Woord=$aZoekwoord[$i];
			if (empty($Quote))
			{	// zoeken naar quote:
				// eerst checken op +-
				if (substr($Woord,0,1)=="-")
				{	$Min="-";
					$Woord=substr($Woord,1);
				}
				else
				{	$Min="";
					if (substr($Woord,0,1)=="+"){$Woord=substr($Woord,1);} //+ schrappen
				}
					
				if (substr($Woord,0,1)=='"' or substr($Woord,0,1)=="'")
				{	$Quote=substr($Woord,0,1);
					$aZoekwoord[$i]=$Min.substr($Woord,1);
					$Min="";
					--$i; // zoekwoord nogmaals meenemen voor verwerking
				}
				else
				{	array_push($aZoekveld,addslashes($Min.$Woord));
				}
			}
			else
			{	// zoeken naar eind quote:
				if (substr($Woord,strlen($Woord)-1,1)==$Quote)
				{	$WordQ.=" ".substr($Woord,0,strlen($Woord)-1);
					array_push($aZoekveld,trim(addslashes($WordQ)));
					$WordQ="";
					$Quote="";
				}
				else
				{	$WordQ.=" ".$Woord;
				}
			}
		}
		if (!empty($WordQ)){array_push($aZoekveld,$WordQ);} // laatste tot eind ook meenemen
		return $aZoekveld;
	}

}